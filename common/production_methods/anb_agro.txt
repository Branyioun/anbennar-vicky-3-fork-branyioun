﻿pm_mushrooms = {
	texture = "gfx/interface/icons/production_method_icons/potatoes.dds"
	
	building_modifiers = {
		workforce_scaled = {
			building_output_grain_add = -20
			building_output_opium_add = 9
		}
	}
}