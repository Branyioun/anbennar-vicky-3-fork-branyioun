﻿# Like normal landholders, but they dislike other races
ideology_greentide_adventurers = {	
	lawgroup_racial_tolerance = {
		law_same_race_only = approve
		law_giantkin_group_only = disapprove
		law_goblinoid_group_only = disapprove
		law_giantkin_group_and_humans = disapprove
		law_goblinoid_group_and_humans = disapprove
		law_monstrous_only = strongly_disapprove
		law_non_monstrous_only = strongly_approve
		law_all_races_allowed = strongly_disapprove
	}
	
	lawgroup_army_model = {
		law_peasant_levies = approve
		law_professional_army = neutral
		law_mass_conscription = neutral
		law_national_militia = strongly_disapprove
	}
	
	lawgroup_land_reform = {
		law_serfdom = approve
		law_tenant_farmers = neutral
		law_commercialized_agriculture = disapprove
		law_homesteading = disapprove
		law_collectivized_agriculture = disapprove
	}
	
	lawgroup_taxation = {
		law_consumption_based_taxation = approve
		law_land_based_taxation = approve
		law_per_capita_based_taxation = neutral
		law_proportional_taxation = disapprove
		law_graduated_taxation = strongly_disapprove
	}

	lawgroup_welfare = {
		law_no_social_security = approve
		law_poor_laws = neutral
		law_wage_subsidies = disapprove
		law_old_age_pension = disapprove
	}
}

ideology_baronites_paternalistic = {
	icon = "gfx/interface/icons/ideology_icons/junker_paternalistic.dds"
	
	lawgroup_governance_principles = {
		law_monarchy = strongly_approve
		law_theocracy = approve			
		law_presidential_republic = neutral
		law_parliamentary_republic = neutral
		law_council_republic = strongly_disapprove

		#Anbennar
		law_magocracy = approve
	}
	
	lawgroup_distribution_of_power = {
		law_single_party_state = neutral
		law_landed_voting = strongly_approve
		law_autocracy = strongly_approve
		law_oligarchy = approve
		law_wealth_voting = neutral
		law_census_voting = disapprove
		law_universal_suffrage = strongly_disapprove
		law_anarchy = strongly_disapprove
		law_technocracy = strongly_disapprove
	}
	
	lawgroup_bureaucracy = {
		law_hereditary_bureaucrats = approve
		law_appointed_bureaucrats = disapprove
		law_elected_bureaucrats = approve
	}

	lawgroup_policing = {
		law_local_police = approve
		law_dedicated_police = approve
		law_militarized_police = strongly_disapprove
		law_no_police = disapprove
	}
}

#Likes paternalistic but EVIL
ideology_witch_king_likers = {	
	lawgroup_governance_principles = {
		law_monarchy = strongly_approve
		law_theocracy = approve
		law_presidential_republic = neutral
		law_parliamentary_republic = neutral
		law_council_republic = strongly_disapprove

		#Anbennar
		law_magocracy = strongly_approve
	}
	
	lawgroup_distribution_of_power = {
		law_single_party_state = neutral
		law_landed_voting = strongly_approve
		law_autocracy = strongly_approve
		law_oligarchy = approve
		law_wealth_voting = neutral
		law_census_voting = disapprove
		law_universal_suffrage = strongly_disapprove
		law_anarchy = strongly_disapprove
		law_technocracy = strongly_disapprove
	}
	
	lawgroup_bureaucracy = {
		law_hereditary_bureaucrats = approve
		law_appointed_bureaucrats = neutral
		law_elected_bureaucrats = disapprove
	}

	lawgroup_policing = {
		law_local_police = approve
		law_dedicated_police = neutral
		law_militarized_police = neutral
		law_no_police = disapprove
	}
	
	lawgroup_economic_system = {		
		law_agrarianism = strongly_approve
		law_traditionalism = approve
		law_interventionism = neutral
		law_laissez_faire = disapprove
		law_command_economy = strongly_disapprove
	}
	
	lawgroup_trade_policy = {		
		law_isolationism = approve
		law_mercantilism = approve
		law_protectionism = neutral
		law_free_trade = disapprove
	}
	
	lawgroup_magic_and_artifice = {
		law_traditional_magic_only = strongly_approve
		law_traditional_magic_encouraged = approve
		law_artifice_encouraged = disapprove
		law_artifice_only = strongly_disapprove
		law_mundane_production = strongly_disapprove
	}
	
	lawgroup_mage_ethics = {
		law_dark_arts_banned = disapprove
		law_pragmatic_application = neutral
		law_dark_arts_embraced = approve
	}
}


#Likes free artifice, dislikes magic, generally ok with some evilness
ideology_artifice_supremacist = {
	lawgroup_artificer_ethics = {
		law_amoral_artifice_banned = approve
		law_pragmatic_artifice = approve
		law_amoral_artifice_embraced = neutral
	}
	
	lawgroup_magic_and_artifice = {
		law_traditional_magic_only = strongly_approve
		law_traditional_magic_encouraged = approve
		law_artifice_encouraged = neutral
		law_artifice_only = strongly_disapprove
		law_mundane_production = strongly_disapprove
	}
}

#Likes Magic
ideology_magical_supremacist = {
	lawgroup_magic_and_artifice = {
		law_traditional_magic_only = strongly_approve
		law_traditional_magic_encouraged = approve
		law_artifice_encouraged = disapprove
		law_artifice_only = strongly_disapprove
		law_mundane_production = strongly_disapprove
	}
}