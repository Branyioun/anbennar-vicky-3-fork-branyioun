﻿#Make sure to update on_river scripted_trigger after every new river trait

state_trait_oddanroy_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 30
	}
}

state_trait_alen_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 40
	}
}

state_trait_widderoy_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 30
	}
}

state_trait_middanroy_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 30
	}
}

state_trait_sornroy_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 20
	}
}

state_trait_luna_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 20
	}
}

state_trait_esmar_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 30
	}
}

state_trait_bloodwine_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 20
	}
}

state_trait_dostanesck_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 30
	}
}

state_trait_rafn_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 20
	}
}

state_trait_anbenncost = {
	icon = "gfx/interface/icons/state_trait_icons/natural_harbors.dds"
	
	modifier = {
		state_infrastructure_add = 30
		state_migration_pull_mult = 0.5
	}
}

state_trait_magically_drained_lands = { #quasi-placeholder. gained via eilisin MT
	icon = gfx/interface/icons/timed_modifier_icons/modifier_gear_negative.dds
	
	modifier = {
		building_group_bg_agriculture_throughput_mult = -0.2
		building_group_bg_logging_throughput_mult = -0.2
	}
}

state_trait_verdancy_weed_outbreak = { #verdancy aka magical overgrowths due to magical overexperimentation
	icon = gfx/interface/icons/timed_modifier_icons/modifier_gear_negative.dds
	
	modifier = {
		building_group_bg_agriculture_throughput_mult = -0.2
		building_group_bg_logging_throughput_mult = -0.2
	}
}

state_trait_magically_enriched_soils = {	#quasi-placeholder. gained via eilisin MT
	icon = "gfx/interface/icons/state_trait_icons/good_soils.dds"
	
	modifier = {
		building_group_bg_agriculture_throughput_mult = 0.2
	}
}

state_trait_dalrfjall_prospecting = {
	icon = "gfx/interface/icons/state_trait_icons/mountain.dds"
	
	modifier = {
		state_construction_mult = -0.15
		state_infrastructure_mult = -0.15
		building_iron_mine_throughput_mult = 0.2
		building_lead_mine_throughput_mult = 0.2
	}
}

#Ashwheat from EU4 ideas of Asheniande
state_trait_ashwheat_fields = {	
	icon = "gfx/interface/icons/state_trait_icons/good_soils.dds"
	
	modifier = {
		building_wheat_farm_throughput_mult = 0.3
	}
}

state_trait_terrman_flower_beatles_dwindling = {	#TODO: journal entry to restore to abundant or just remove malus
	icon = "gfx/interface/icons/state_trait_icons/good_soils.dds"
	
	modifier = {
		building_dye_plantation_throughput_mult = -0.2
	}
}

state_trait_terrman_flower_beatles_abundant = {
	icon = "gfx/interface/icons/state_trait_icons/good_soils.dds"
	
	modifier = {
		building_dye_plantation_throughput_mult = 0.2
	}
}

state_trait_greatwoods = {
	icon = "gfx/interface/icons/state_trait_icons/resources_lumber.dds"
	
	modifier = {
		building_group_bg_logging_throughput_mult = 0.2
	}
}

state_trait_titans_mountains = {
	icon = "gfx/interface/icons/state_trait_icons/mountain.dds"
	
	modifier = {
		state_construction_mult = -0.2
		state_infrastructure_mult = -0.2
	}
}

