﻿state_trait_egoirlust_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		building_group_bg_agriculture_throughput_mult = 0.1
		state_infrastructure_add = 20
	}
}

state_trait_valak_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 20
	}
}

state_trait_kozyoshdek_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 20
	}
}

state_trait_volutsabaj_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 20
	}
}

state_trait_mudholkhin_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 20
	}
}

state_trait_kalyins_gift = {
	icon = "gfx/interface/icons/state_trait_icons/good_soils.dds"
	
	modifier = {
		building_group_bg_agriculture_throughput_mult = 0.10
		building_group_bg_plantations_throughput_mult = 0.10
	}
}

state_trait_ultakal_mines = {
	icon = "gfx/interface/icons/state_trait_icons/resources_ore.dds"
	
	modifier = {
		building_coal_mine_throughput_mult = 0.1
		building_gold_mine_throughput_mult = 0.1
	}
}

state_trait_akandhil_mineral_fields = {
	icon = "gfx/interface/icons/state_trait_icons/resources_ore.dds"
	
	modifier = {
		building_iron_mine_throughput_mult = 0.1
		building_coal_mine_throughput_mult = 0.1
	}
}

state_trait_dzimokli_coal_fields = {
	icon = "gfx/interface/icons/state_trait_icons/resources_ore.dds"
	
	modifier = {
		building_coal_mine_throughput_mult = 0.1
	}
}