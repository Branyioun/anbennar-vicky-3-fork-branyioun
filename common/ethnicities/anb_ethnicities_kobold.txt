﻿@neg1_min = 0.4
@neg1_max = 0.5

@neg2_min = 0.3
@neg2_max = 0.4

@neg3_min = 0.1
@neg3_max = 0.3

@pos1_min = 0.5
@pos1_max = 0.6

@pos2_min = 0.6
@pos2_max = 0.7

@pos3_min = 0.7
@pos3_max = 0.9

@beauty1min = 0.35
@beauty1max = 0.65

@beauty2min = 0.4
@beauty2max = 0.6

@beauty3min = 0.45
@beauty3max = 0.55

@blend1min = 0.0
@blend1max = 0.2

@blend2min = 0.2
@blend2max = 0.5

@blend3min = 0.5
@blend3max = 0.8

kobold_rgb = {
	template = "halfling"	#lol. just for a bit of fun and to differentiate them for now

	skin_color = {
		10 = { 0.0 0.53 0.05 0.6 }   #red
        10 = { 0.51 0.53 0.6 0.6 }   #green
        10 = { 0.75 0.55 0.8 0.6 }   #blue
	}

    #Anbennar - nosferatu bat noses from PoD
    POD_NOSschnoz = {
        10 = { name = nosies            range = { 0.8 1.0 } }
        10 = { name = schnozies            range = { 0.8 1.0 } }
    }
}

kobold_gold = {
	template = "halfling"	#lol. just for a bit of fun and to differentiate them for now

	skin_color = {
		10 = { 0.25 0.6 0.35 0.75 }  
	}

    #Anbennar - nosferatu bat noses from PoD
    POD_NOSschnoz = {
        10 = { name = nosies            range = { 0.8 1.0 } }
        10 = { name = schnozies            range = { 0.8 1.0 } }
    }
}
	