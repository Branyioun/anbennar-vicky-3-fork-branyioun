INTERESTS = {
	c:A01 = { #Anbennar
		add_declared_interest = region_adenica
		add_declared_interest = region_dostanor
		add_declared_interest = region_amadia
		add_declared_interest = region_north_lencenor
	}
	
	c:A02 = { #Vivin Empire - Just regions in their general vicinity
		add_declared_interest = region_the_marches
		add_declared_interest = region_bahar
		add_declared_interest = region_businor
		add_declared_interest = region_adenica
	}
	
	c:A03 = { #Lorent
		add_declared_interest = region_northern_dameshead
		add_declared_interest = region_dragon_coast
		add_declared_interest = region_eastern_dameshead
		add_declared_interest = region_businor
		add_declared_interest = region_south_rahen
		add_declared_interest = region_thidinkai
		add_declared_interest = region_lupulan
	}

	c:A04 = { #Northern League
		add_declared_interest = region_bulwar_proper
		add_declared_interest = region_adenica
		add_declared_interest = region_north_lencenor
	}
}
