﻿COUNTRIES = {
	c:A02 = {
		effect_starting_technology_tier_3_tech = yes
		
		effect_starting_politics_traditional = yes
		
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_colonial_resettlement
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_artifice_encouraged	#due to ravelian influence

		ig:ig_landowners = {
			add_ruling_interest_group = yes
		}
		ig:ig_devout = {
			set_interest_group_name = ig_ravelian_church
			remove_ideology = ideology_moralist
			add_ideology = ideology_ravelian_moralist
			remove_ideology = ideology_pious
			add_ideology = ideology_scholarly
		}
		
		add_journal_entry = { type = je_settle_the_folly }
	}
}