﻿COUNTRIES = {
	c:A24 = {
		effect_starting_technology_tier_3_tech = yes
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_oligarchy	#lots of power to newshire landowning families, different to small country
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_dedicated_police	#as per newshire sherrifs
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_censorship
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property #Not allowed women in workplace without voting
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_mundane_production	#they hate magic due to verdancy
	
		ig:ig_landowners = {
			remove_ideology = ideology_stratocratic
			add_ideology = ideology_greentide_adventurers
		}
		ig:ig_devout = {
			set_interest_group_name = ig_corinite_faithful
			remove_ideology = ideology_moralist
			add_ideology = ideology_corinite_moralist
			remove_ideology = ideology_patriarchal
			add_ideology = ideology_feminist_ig
			add_ideology = ideology_anti_slavery
		}
	}
}