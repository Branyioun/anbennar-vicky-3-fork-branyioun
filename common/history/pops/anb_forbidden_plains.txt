POPS = {
	s:STATE_SHIK_DAZAR = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 3250000
			}
		}
	}
	s:STATE_ZOI_KHORKHIIN = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 1356200
			}
			create_pop = {
				culture = metsamic
				size = 433800
			}
		}
	}
	s:STATE_SHIKENKHIIN = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 4440000
			}
		}
	}
	s:STATE_SARTZ_NEISAR = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 727500
			}
			create_pop = {
				culture = metsamic
				size = 2182500
			}
		}
	}
	s:STATE_KESH_GOLKHIN = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 4167400
			}
			create_pop = {
				culture = metsamic
				size = 1866000
			}
			create_pop = {
				culture = zabatlari
				size = 186600
			}
		}
	}
	s:STATE_QUSHYIL = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 561600
			}
			create_pop = {
				culture = zabatlari
				size = 1778400
			}
		}
	}
	s:STATE_YUKAROYOL = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 597600
			}
			create_pop = {
				culture = zabatlari
				size = 2456800
			}
			create_pop = {
				culture = metsamic
				size = 265600
			}
		}
	}
	s:STATE_YUKARON = {
		region_state:E01 = {
			create_pop = {
				culture = zabatlari
				size = 2476500
			}
			create_pop = {
				culture = metsamic
				size = 1333500
			}
		}
	}
	s:STATE_PRIKOYOL = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 71000
			}
			create_pop = {
				culture = zabatlari
				size = 3195000
			}
			create_pop = {
				culture = metsamic
				size = 106500
			}
			create_pop = {
				culture = orachav
				size = 142000
			}
			create_pop = {
				culture = fathide_ogre
				size = 17750
			}
			create_pop = {
				culture = centaur
				size = 17750
			}
		}
	}
	s:STATE_GADHLUMO = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 21550
			}
			create_pop = {
				culture = metsamic
				size = 4266900
			}
			create_pop = {
				culture = kukatodic
				size = 21550
			}
		}
	}
	s:STATE_PEENADHI = {
		region_state:E01 = {
			create_pop = {
				culture = zabatlari
				size = 292000
			}
			create_pop = {
				culture = metsamic
				size = 2628000
			}
		}
	}
	s:STATE_ZUURZOI = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 15500
			}
			create_pop = {
				culture = urmanki
				size = 294500
			}
		}
	}
	s:STATE_ZOITAL = {
		region_state:E01 = {
			create_pop = {
				culture = orachav
				size = 10140
			}
			create_pop = {
				culture = urmanki
				size = 158860
			}
		}
	}
	s:STATE_TOZGONREK = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 61500
			}
			create_pop = {
				culture = orachav
				size = 36900
			}
			create_pop = {
				culture = urmanki
				size = 1131600
			}
		}
	}
	s:STATE_AKANDHIL = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 5520
			}
			create_pop = {
				culture = zabatlari
				size = 4600
			}
			create_pop = {
				culture = orachav
				size = 4600
			}
			create_pop = {
				culture = urmanki
				size = 77280
			}
		}
	}
	s:STATE_BULREKAYIG = {
		region_state:E01 = {
			create_pop = {
				culture = khamgunai
				size = 40400
			}
			create_pop = {
				culture = zabatlari
				size = 222200
			}
			create_pop = {
				culture = orachav
				size = 101000
			}
			create_pop = {
				culture = urmanki
				size = 1656400
			}
		}
	}
	s:STATE_ZARMIKLON = {
		region_state:E01 = {
			create_pop = {
				culture = zabatlari
				size = 153000
			}
			create_pop = {
				culture = urmanki
				size = 1377000
			}
		}
	}
	s:STATE_APORLAEN = {
		region_state:E04 = {
			create_pop = {
				culture = zabatlari
				size = 157300
			}
			create_pop = {
				culture = orachav
				size = 843700
			}
			create_pop = {
				culture = urmanki
				size = 429000
			}
		}
	}
	s:STATE_ORCHEKH = {
		region_state:E04 = {
			create_pop = {
				culture = zabatlari
				size = 95400
			}
			create_pop = {
				culture = metsamic
				size = 95400
			}
			create_pop = {
				culture = orachav
				size = 1272000
			}
			create_pop = {
				culture = urmanki
				size = 127200
			}
		}
	}
	s:STATE_ADOI_FILEANAN = {
		region_state:E03 = {
			create_pop = {
				culture = metsamic
				size = 42200
			}
			create_pop = {
				culture = orachav
				size = 1856800
			}
			create_pop = {
				culture = urmanki
				size = 105500
			}
			create_pop = {
				culture = centaur
				size = 105500
			}
		}
	}
	s:STATE_SOCHULEAG = {
		region_state:E03 = {
			create_pop = {
				culture = metsamic
				size = 52000
			}
			create_pop = {
				culture = orachav
				size = 738400
			}
			create_pop = {
				culture = kukatodic
				size = 52000
			}
			create_pop = {
				culture = urmanki
				size = 156000
			}
			create_pop = {
				culture = centaur
				size = 41600
			}
		}
	}
	s:STATE_NAGLAIBAR = {
		region_state:E08 = {
			create_pop = {
				culture = centaur
				size = 805000
			}
		}
		region_state:E03 = {
			create_pop = {
				culture = orachav
				size = 91700
			}
			create_pop = {
				culture = centaur
				size = 20300
			}
		}
	}
	s:STATE_SARLUN_GOILUST = {
		region_state:E03 = {
			create_pop = {
				culture = orachav
				size = 215000
			}
			create_pop = {
				culture = centaur
				size = 35000
			}
		}
		region_state:E05 = {
			create_pop = {
				culture = centaur
				size = 180000
			}
		}
	}
	s:STATE_CAUBHEAMEAS = {
		region_state:E06 = {
			create_pop = {
				culture = centaur
				size = 100100
			}
		}
		region_state:E02 = {
			create_pop = {
				culture = fathide_ogre
				size = 18900
			}
			create_pop = {
				culture = centaur
				size = 7000
			}
		}
	}
	s:STATE_FOIRAKHIAN = {
		region_state:E02 = {
			create_pop = {
				culture = fathide_ogre
				size = 265600
			}
			create_pop = {
				culture = centaur
				size = 33200
			}
			create_pop = {
				culture = cave_goblin
				size = 33200
			}
		}
	}
	s:STATE_SHEVROMRZGH = {
		region_state:E02 = {
			create_pop = {
				culture = fathide_ogre
				size = 592000
			create_pop = {
				culture = centaur
				size = 37000
			}
			create_pop = {
				culture = cave_goblin
				size = 111000
			}
		}
	}
	s:STATE_SKURKHA_KYARD = {
		region_state:E02 = {
			create_pop = {
				culture = fathide_ogre
				size = 744000
			}
			create_pop = {
				culture = cave_goblin
				size = 186000
			}
		}
	}
	s:STATE_OVTO_KIGVAL = {
		region_state:E02 = {
			create_pop = {
				culture = fathide_ogre
				size = 780000
			}
			create_pop = {
				culture = cave_goblin
				size = 260000
			}
		}
	}
	s:STATE_KVAKEINOLBA = {
		region_state:E02 = {
			create_pop = {
				culture = fathide_ogre
				size = 805000
			}
			create_pop = {
				culture = cave_goblin
				size = 345000
			}
		}
	}
	s:STATE_DZIMOKLI = {
		region_state:E02 = {
			create_pop = {
				culture = fathide_ogre
				size = 900000
			}
			create_pop = {
				culture = cave_goblin
				size = 600000
			}
		}
	}
	s:STATE_GHANEERSP = {
		region_state:E09 = {
			create_pop = {
				culture = centaur
				size = 1323000
			}
			create_pop = {
				culture = orachav
				size = 147000
			}
		}
	}
	s:STATE_UGHEABAR = {
		region_state:E08 = {
			create_pop = {
				culture = centaur
				size = 700000
			}
		}
	}
	s:STATE_IRDU_AGEENEAS = {
		region_state:E07 = {
			create_pop = {
				culture = centaur
				size = 88000
				split_religion = {
					centaur = {
						the_jadd = 1
					}
				}
			}
		}
	}
	s:STATE_SERPENT_GIFT = {
		region_state:E05 = {
			create_pop = {
				culture = centaur
				size = 1770000
			}
		}
	}
	s:STATE_MOITSADHI = {
		region_state:A10 = {
			create_pop = {
				culture = kukatodic
				size = 398544
			}
			create_pop = {
				culture = grombari_half_orc
				size = 26050
			}
			create_pop = {
				culture = grombari_orc
				size = 1122
			}
			create_pop = {
				culture = black_orc
				size = 1414
				split_religion = {
					black_orc = {
						old_dookan = 0.1
						corinite = 0.9
					}
				}
			}
		}
	}
	s:STATE_MOITSA = {
		region_state:A10 = {
			create_pop = {
				culture = kukatodic
				size = 172250
			}
			create_pop = {
				culture = grombari_half_orc
				size = 97750
			}
			create_pop = {
				culture = grombari_orc
				size = 51819
			}
			create_pop = {
				culture = black_orc
				size = 1849
				split_religion = {
					black_orc = {
						old_dookan = 0.1
						corinite = 0.9
					}
				}
			}
		}
	}
	s:STATE_YYL_MOITSA = {
		region_state:A10 = {
			create_pop = {
				culture = kukatodic
				size = 56421
			}
			create_pop = {
				culture = grombari_half_orc
				size = 5450
			}
			create_pop = {
				culture = grombari_orc
				size = 75
			}
		}
	}
}