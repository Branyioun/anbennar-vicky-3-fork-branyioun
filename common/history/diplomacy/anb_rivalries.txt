﻿DIPLOMACY = {
	c:A01 = {	#Anbennar-Lorent
		create_diplomatic_pact = {
			country = c:A03
			type = rivalry
		}
	}
	c:A03 = {	#Lorent-Northern League
		create_diplomatic_pact = {
			country = c:A04
			type = rivalry
		}
	}		
	c:A04 = {	#Northern League-Grombar
		create_diplomatic_pact = {
			country = c:A10
			type = rivalry
		}
	}	
	c:B29 = {	#Sarda-Dragon Dominion
		create_diplomatic_pact = {
			country = c:B49
			type = rivalry
		}
	}

	c:B27 = { #Neratica-Trollsbay
		create_diplomatic_pact = {
			country = c:B19
			type = rivalry
		}
	}

	c:B41 = {	#Plumstead is still salty they were forced to release Beggaston
		create_diplomatic_pact = {
			country = c:B34
			type = rivalry
		}
	}

	c:B34 = {	#Beggastonic is still salty about being occupied during the Decades of the Mountain
		create_diplomatic_pact = {
			country = c:B41
			type = rivalry
		}
	}
}	




