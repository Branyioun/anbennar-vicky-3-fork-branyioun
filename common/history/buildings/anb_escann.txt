﻿BUILDINGS={
	s:STATE_BALMIRE={ # Tis a marsh
		region_state:A04={
			create_building={
				building="building_government_administration"
				level=5
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_vineyards" "pm_tools_disabled" "pm_soil_enriching_farming" }
			}
		}
	}
	
	s:STATE_ADENICA = {
		region_state:A22 = {
			#Adenican livestock
			create_building={
				building="building_livestock_ranch"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_wheat_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_vineyards" "pm_tools_disabled" "pm_soil_enriching_farming" }
			}
			# For their guns, not enough to cover their needs tho so they'll have to import
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_barracks"
				level=20
				reserves=1
				activate_production_methods={ "pm_line_infantry" "pm_cannon_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	
	s:STATE_ROHIBON = {
		region_state:A22 = {
			create_building={
				building="building_livestock_ranch"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_vineyards" "pm_tools_disabled" "pm_soil_enriching_farming" }
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_line_infantry" "pm_cannon_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_government_administration"
				level=5
				reserves=1
			}
		}
	}
	
	s:STATE_ANCARDIAN_PLAINS = {
		region_state:A22 = {
			#Very basic adminstration
			create_building={
				building="building_government_administration"
				level=7
				reserves=1
			}
			# Make em more gunny
			create_building={
				building="building_arms_industry"
				level=3
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry" "pm_cannons" }
			}
			create_building={
				building="building_munition_plants"
				level=2
				reserves=1
				activate_production_methods={ "pm_percussion_caps" "pm_merchant_guilds_building_munition_plants" }
			}
			create_building={
				building="building_food_industry"
				level=2
				reserves=1
				activate_production_methods={ "pm_sweeteners" "pm_disabled_canning" "pm_merchant_guilds_building_food_industry" "pm_manual_dough_processing" "pm_disabled_distillery" }
			}	
			#Army is v big
			create_building={
				building="building_barracks"
				level=30
				reserves=1
				activate_production_methods={ "pm_line_infantry" "pm_cannon_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			#For military use only, pls do not use for making civilian factories ty
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
			}	
			#Halflings work there
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_soil_enriching_farming" "pm_tools_disabled" "pm_citrus_orchards" }
			}
		}
	}

	s:STATE_FARRANEAN = {
		# This region does not have iron anymore
		region_state:A22 = {
			# # Iron for guns, doc says they're short on resources tho so this is it
			# create_building={
				# building="building_iron_mine"
				# level=1
				# reserves=1
			# }
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_line_infantry" "pm_cannon_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
		# region_state:A20 = {
			# create_building={
				# building="building_iron_mine"
				# level=1
				# reserves=1
				# activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			# }
		# }
	}
	
	s:STATE_MORECED = {
		region_state:A22 = {
			create_building={
				building="building_wheat_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_soil_enriching_farming" "pm_tools_disabled" "pm_citrus_orchards" }
			}
			create_building={
				building="building_barracks"
				level=15
				reserves=1
				activate_production_methods={ "pm_line_infantry" "pm_cannon_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	
	s:STATE_NEWSHIRE = {
		region_state:A24 = {
			# Farms and food
			create_building={
				building="building_wheat_farm"
				level=8
				reserves=1
				activate_production_methods={ "pm_soil_enriching_farming" "pm_tools_disabled" "pm_citrus_orchards" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
		}
	}
	
	s:STATE_OUDMERE = {
		region_state:A25 = {
			# They're traders really, so some minor paper industry for their admin but little else
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
			}
			create_building={
				building="building_wheat_farm"
				level=5
				reserves=1
				activate_production_methods={ "pm_vineyards" "pm_tools" } # They make fancy trader wines
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_iron_mine"
				level=2
				reserves=1
			}
		}
	}
	
	s:STATE_DEVACED = {
		region_state:A27 = {
			create_building={
				building="building_wheat_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_vineyards" "pm_tools" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_iron_mine"
				level=3
				reserves=1
			}
		}
	}
	
	s:STATE_VERNHAM = {
		#Lots of food for a big army
		region_state:A27 = {
			# Better adminstration cause they're not a separatist
			create_building={
				building="building_government_administration"
				level=5
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
			}
			create_building={
				building="building_wheat_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_vineyards" "pm_tools" }
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_line_infantry" "pm_cannon_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			#Moved from Vernham cause Marrhold has more industry themes
			create_building={
				building="building_tooling_workshops"
				level=2
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_merchant_guilds_building_tooling_workshops" }
			}
			#Old General academy
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			#Bladedancing stuff, cause that's retro now
			create_building={
				building="building_arts_academy"
				level=1
				reserves=1
				activate_production_methods={ "pm_traditional_art" }
			}
		}
	}
	
	s:STATE_DOSTANS_WAY = {
		region_state:A27 = {
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_wheat_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_vineyards" "pm_tools" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}
	
	s:STATE_BLADEMARCH = {
		region_state:A27 = {
			create_building={
				building="building_government_administration"
				level=3
				reserves=1
			}
			# Make furniture from their wood I guess
			create_building={
				building="building_furniture_manufacturies"
				level=1
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_luxury_furniture" "pm_automation_disabled" "pm_merchant_guilds_building_furniture_manufacturies" }
			}
		}
	}
	
	s:STATE_SILVERVORD = {
		region_state:A26 = {
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_line_infantry" "pm_cannon_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_rye_farm"
				level=3
				reserves=1
			}
		}
	}
	
	s:STATE_CANNWOOD = {
		region_state:A26 = {
			create_building={
				building="building_rye_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_potatoes" "pm_tools_disabled" }
			}
		}
	}
	
	s:STATE_MIDDLE_ALEN = {
		region_state:A10={
			create_building={
				building="building_furniture_manufacturies"
				level=2
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_merchant_guilds_building_furniture_manufacturies" "pm_automation_disabled" "pm_luxury_furniture" }
			}
			create_building={
				building="building_barracks"
				level=20
				reserves=1
				activate_production_methods={ "pm_wound_dressing" "pm_line_infantry" "pm_cavalry_scouts" "pm_no_specialists" "pm_cannon_artillery" }
			}
			create_building={
				building="building_logging_camp"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_rye_farm"
				level=8
				reserves=1
				activate_production_methods={ "pm_apple_orchards" "pm_tools_disabled" }
			}
		}
	}
	
	s:STATE_EBONMARCK = {
		region_state:A10={
			# Tis a forest
			create_building={
				building="building_logging_camp"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_rye_farm"
				level=6
				reserves=1
				activate_production_methods={ "pm_potatoes" "pm_tools_disabled" }
			}
		}
	}
	
	s:STATE_BURNOLL = {
		region_state:A32={
			#No clue what these guys are about so I've left em with just a barracks and a farm. Maybe they're meant to be food
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_vineyards" "pm_tools_disabled" } # They make fancy Rosander wines
			}
		}
		region_state:A28={
			create_building={
				building="building_wheat_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_vineyards" "pm_tools_disabled" }
			}
		}
	}
	
	s:STATE_ROSANVORD = {
		region_state:A28={
			create_building={
				building="building_wheat_farm"
				level=5
				reserves=1
				activate_production_methods={ "pm_vineyards" "pm_tools_disabled" } # They make fancy Rosander wines
			}
			create_building={
				building="building_government_administration"
				level=3
				reserves=1
			}
			create_building={
				building="building_barracks"
				level=15
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	
	s:STATE_MEDIRLEIGH = {
		region_state:A28={
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_wheat_farm"
				level=4
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}
	
	s:STATE_HORNWOOD = {
		region_state:A29={
			create_building={
				building="building_wheat_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_apple_orchards" "pm_tools_disabled" "pm_soil_enriching_farming" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_livestock_ranch"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}
	
	s:STATE_DRYADSDALE = {
		# They do some industry stuff in their MT, this makes more sense than giving them steel
		region_state:A29={
			create_building={
				building="building_food_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_sweeteners" "pm_disabled_canning" "pm_merchant_guilds_building_food_industry" "pm_manual_dough_processing" "pm_pot_stills" }
			}
			create_building={
				building="building_wheat_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_vineyards" "pm_tools_disabled" "pm_soil_enriching_farming" }
			}
		}
	}
	
	s:STATE_MARRVALE = {
		region_state:A29={
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
			}
			create_building={
				building="building_arms_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry" "pm_cannons" }
			}
			create_building={
				building="building_rye_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_potatoes" "pm_tools_disabled" "pm_soil_enriching_farming" }
			}
			#Griffon meat???
			create_building={
				building="building_livestock_ranch"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			#Move these into Marrhold?
			create_building={
				building="building_barracks"
				level=25
				reserves=1
				activate_production_methods={ "pm_line_infantry" "pm_cannon_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_government_administration"
				level=3
				reserves=1
			}
		}
	}
	
	s:STATE_ESSHYL = {
		region_state:A29={
			#Not a core territory so they had to make admin here to legitimize it
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_rye_farms"
				level=2
				reserves=1
				activate_production_methods={ "pm_citrus_orchards" "pm_tools_disabled" "pm_soil_enriching_farming" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}
	
	s:STATE_UNGULDAVOR = { #Doc says they don't utilize their land well
		region_state:A31={
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_rye_farm"
				level=1
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
		}
	}
	
	s:STATE_KONDUNN = { #Doc says they don't utilize their land well
		region_state:A31={
			create_building={
				building="building_rye_farm"
				level=1
				reserves=1
			}
		}
	}
	
	s:STATE_CASTONATH = { 
		region_state:A30={
			#Paper!!!!!
			create_building={
				building="building_paper_mills"
				level=4
				reserves=1
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_merchant_guilds_building_tooling_workshops" }
			}
			#Ex-heart of Escann
			create_building={
				building="building_government_administration"
				level=5
				reserves=1
				activate_production_methods = { "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_glassworks"
				level=1
				reserves=1
				activate_production_methods={ "pm_forest_glass"  "pm_disabled_ceramics" "pm_manual_glassblowing" "pm_privately_owned_building_glassworks" }
			}
			create_building={
				building="building_barracks"
				level=30
				reserves=1
				activate_production_methods={ "pm_line_infantry" "pm_cannon_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_food_industry"
				level=3
				reserves=1
				activate_production_methods={ "pm_bakeries" "pm_disabled_canning" "pm_merchant_guilds_building_food_industry" "pm_manual_dough_processing" "pm_disabled_distillery" }
			}
			create_building={
				building="building_rye_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_soil_enriching_farming" "pm_tools_disabled" "pm_citrus_orchards" }
			}
		}
	}
	
	s:STATE_LOWER_NATH = { 
		region_state:A30={
			#Esthil builds farms here
			create_building={
				building="building_rye_farm"
				level=6
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_line_infantry" "pm_cannon_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	
	s:STATE_WESTGATE = { 
		region_state:A30={
			create_building={
				building="building_rye_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_soil_enriching_farming" "pm_tools_disabled" "pm_citrus_orchards" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods = { "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_line_infantry" "pm_cannon_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	
	s:STATE_NORTHYL = { 
		region_state:A30={
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_rye_farm"
				level=2
				reserves=1
			}
			create_building={
				building="building_damestear_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_damestear_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_damestear_mine" }
			}
		}
	}
	
	s:STATE_SERPENTSMARCK = { 
		region_state:A30={
			create_building={
				building="building_rye_farm"
				level=1
				reserves=1
			}
			create_building={
				building="building_iron_mine"
				level=2
				reserves=1
			}
		}
	}
	
	s:STATE_STEELHYL = {
		region_state:A30={
			create_building={
				building="building_iron_mine"
				level=2
				reserves=1
			}
			#Where the guns used to be made
			create_building={
				building="building_arms_industry"
				level=2
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry" "pm_cannons" }
			}
			create_building={
				building="building_munition_plants"
				level=1
				reserves=1
				activate_production_methods={ "pm_percussion_caps" "pm_merchant_guilds_building_munition_plants" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}
	
	s:STATE_EASTGATE = { 
		region_state:A30={
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_livestock_ranch"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods = { "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_rye_farm"
				level=2
				reserves=1
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_line_infantry" "pm_cannon_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	
	s:STATE_UPPER_NATH = { 
		region_state:A30={
			create_building={
				building="building_wheat_farm"
				level=3
				reserves=1
			}
			#Corintar has stuff for logging here
			create_building={
				building="building_logging_camp"
				level=3
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
		}
	}
}