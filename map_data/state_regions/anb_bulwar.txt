﻿STATE_REUYEL = {
    id = 300
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4A2CC0" "x4A3480" "xCA2EFF" "xCA3240" "xCA36C0" "xd32a40" }
    traits = { "state_trait_natural_harbors" }
    city = "xca3240"
    port = "x492840"
    farm = "x4a3480"
    mine = "xc92a80"
    wood = "xc92600"
    arable_land = 54
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_silk_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_coal_mining = 10
        bg_logging = 8
        bg_fishing = 6
    }
    naval_exit_id = 3002
}

STATE_MEDBAHAR = {
    id = 301
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4A3000" "x4B40C0" "xCB3A00" "xCB3E80" "xCB42FF" }
    traits = { "state_trait_tungr_mountains" }
    city = "x4b3c40"
    port = ""
    farm = "x4a3000"
    mine = "xcb3a00"
    wood = "xcb42ff"
    arable_land = 30
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_iron_mining = 24
        bg_coal_mining = 14
        bg_logging = 12
        bg_fishing = 3
    }
    naval_exit_id = 3003
}

STATE_MEGAIROUS = {
    id = 302
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4B4400" "x4C4CFF" "xCB4640" "xCC4AC0" "xCC4E00" }
    traits = {}
    city = "xcb4640"
    port = "xcc4ac0"
    farm = "x4b4400"
    mine = "x4c4cff"
    wood = "xcc4e00"
    arable_land = 36
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_coal_mining = 12
        bg_logging = 22
        bg_fishing = 6
    }
    naval_exit_id = 3003
}

STATE_AQATBAHAR = {
    #NEEDS PORT
    id = 303
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4C5040" "x4C54C0" "x4D5800" "x4D60FF" "x5C3400" "xCC5280" "xCD56FF" }
    traits = {}
    city = "x4c5040"
    farm = "x4c54c0"
    mine = "x5c3400"
    wood = "xcc5280"
    arable_land = 60
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_iron_mining = 24
        bg_coal_mining = 16
        bg_logging = 24
        bg_fishing = 4
    }
    naval_exit_id = 3003
}

STATE_BAHAR_PROPER = {
    id = 304
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4C4880" "x4D5C80" "x4E68C0" "x4E6C00" "xCD5A40" "xCD5EC0" "xCD6200" "xCE6680" "xCE6AFF" "xCE6E40" "xCF72C0" "xDC32FF" }
    traits = {}
    city = "xcd5ec0"
    port = "x4d5c80"
    farm = "xce6680"
    mine = "xcf72c0"
    wood = "x4e6c00"
    arable_land = 99
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_coal_mining = 12
        bg_logging = 32
        bg_fishing = 5
    }
    naval_exit_id = 3003
}

STATE_DROLAS = {
    id = 305
    subsistence_building = "building_subsistence_farms"
    provinces = { "x5224C0" "x522800" "x529CFF" "x532C80" "xD19AC0" "xD22280" }
    traits = {}
    city = "x5224c0"
    port = "x522800"
    farm = "xd19ac0"
    mine = "xd22280"
    wood = "x529cff"
    arable_land = 22
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_banana_plantations }
    capped_resources = {
        bg_iron_mining = 14
        bg_logging = 8
        bg_fishing = 10
    }
    naval_exit_id = 3003
}

STATE_KUZARAM = {
    id = 306
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4E7080" "x4F74FF" "x4F7840" "xCF7600" "xCF7A80" }
    traits = { "state_trait_harpy_hills" }
    city = "x4e7080"
    port = "xcf7600"
    farm = "x4f74ff"
    mine = "x4f7840"
    wood = "xcf7a80"
    arable_land = 28
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_iron_mining = 24
        bg_logging = 12
        bg_fishing = 8
    }
    naval_exit_id = 3003
}

STATE_BRASAN = {
    id = 307
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x508480" "x5088FF" "x5190C0" "x519400" "x519880" "xD086C0" "xD08A00" "xD18E80" }
    traits = { "state_trait_suran_river" "state_trait_natural_harbors" }
    city = "xd08a00"
    port = "xd086c0"
    farm = "xd18e80"
    wood = "x519880"
    arable_land = 72
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_silk_plantations bg_tea_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 5
        bg_fishing = 9
    }
    naval_exit_id = 3003
}

STATE_SAD_SUR = {
    id = 308
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x617000" "x617480" "x627C40" "x6280C0" "x628880" "xD54A80" "xE16EFF" "xE176C0" "xE17A00" "xE27E80" "xE282FF" "xE28640" "xE38AC0" }
    traits = { "state_trait_salahad_desert" "state_trait_sad_sur" }
    city = "x6280c0"
    port = "x617000"
    farm = "xd54a80"
    mine = "x617480"
    arable_land = 28
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 12
        bg_lead_mining = 28
        bg_coal_mining = 40
        bg_logging = 5
        bg_fishing = 2
    }
    naval_exit_id = 3003
}

STATE_LOWER_BURANUN = {
    id = 309
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x508000" "x533440" "x5438C0" "xCF7EFF" "xD442C0" }
    traits = { "state_trait_buranun_river" "state_trait_natural_harbors" }
    city = "x508000"
    port = "xcf7eff"
    farm = "x5438c0"
    wood = "xd442c0"
    arable_land = 82
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 5
        bg_fishing = 4
    }
    naval_exit_id = 3003
}

STATE_LOWER_SURAN = {
    id = 310
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x5330FF" "x5444FF" "x554840" "xD08240" "xD54600" }
    traits = { "state_trait_suran_river" }
    city = "xd08240"
    farm = "x554840"
    wood = "xd54600"
    arable_land = 97
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_silk_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 5
    }
}

STATE_BULWAR = {
    id = 311
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x554CC0" "x555000" "x565480" "x5658FF" "x565C40" "xD54EFF" "xD55240" "xD65A00" "xD76640" }
    traits = { "state_trait_buranun_river" "state_trait_suran_river" }
    city = "xd55240"
    farm = "x554cc0"
    arable_land = 207
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_silk_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 2
    }
}

STATE_KUMARKAND = {
    id = 312
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x543C00" "x576400" "x576CFF" "xD43AFF" "xD43E40" "xD656C0" "xD762FF" }
    traits = { "state_trait_buranun_river" }
    city = "xd43e40"
    farm = "x576cff"
    arable_land = 104
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 4
        bg_coal_mining = 22
    }
}

STATE_WEST_NAZA = {
    id = 313
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x5660C0" "x5980FF" "x5988C0" "xD76AC0" "xD76E00" }
    traits = { "state_trait_buranun_river" }
    city = "xd76ac0"
    farm = "x5660c0"
    arable_land = 166
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 3
    }
}

STATE_EAST_NAZA = {
    id = 314
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x587040" "x5874C0" "x587800" "x587C80" "xD87280" "xD876FF" "xD87A40" "xD97EC0" }
    traits = { "state_trait_good_soils" }
    city = "x587040"
    farm = "x587c80"
    mine = "xd97ec0"
    arable_land = 203
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_silk_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_iron_mining = 12
        bg_gold_mining = 2
        bg_logging = 3
    }
}

STATE_JADDANZAR = {
    id = 315
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x576880" "x598440" "x628400" "xD65E80" "xD98200" "xD98680" }
    traits = { "state_trait_suran_river" }
    city = "xd98200"
    farm = "xd65e80"
    arable_land = 234
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_silk_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 2
    }
}

STATE_AVAMEZAN = {
    id = 316
    subsistence_building = "building_subsistence_farms"
    provinces = { "x5A8C00" "x5A9080" "x5A94FF" "x5A9840" "xD98AFF" "xDA8E40" "xDA92C0" "xDA9600" "xDB9A80" }
    traits = { "state_trait_suran_river" }
    city = "xda8e40"
    farm = "x5a8c00"
    mine = "x5a9080"
    arable_land = 113
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_silk_plantations bg_tobacco_plantations bg_coffee_plantations }
    capped_resources = {
        bg_iron_mining = 12
        bg_logging = 3
    }
}

STATE_UPPER_SURAN = {
    id = 317
    subsistence_building = "building_subsistence_farms"
    provinces = { "x5B2000" "x5B9CC0" "xDB2240" "xDB9EFF" }
    traits = { "state_trait_suran_river" }
    city = "x5b2000"
    farm = "xdb9eff"
    arable_land = 53
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_silk_plantations bg_tobacco_plantations bg_coffee_plantations }
    capped_resources = {
        bg_logging = 2
        bg_sulfur_mining = 4
    }
}

STATE_AZKA_SUR = {
    id = 318
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4F7CC0" "x5B2480" "x5C28FF" "x5C2C40" "x5C30C0" "xD33200" "xDB26C0" "xDC2A00" "xDC2E80" }
    traits = { "state_trait_suran_river" "state_trait_salahad_desert" }
    city = "xdb26c0"
    farm = "x5b2480"
    mine = "x4f7cc0"
    arable_land = 81
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_opium_plantations bg_coffee_plantations }
    capped_resources = {
        bg_iron_mining = 12
        bg_sulfur_mining = 16
        bg_logging = 2
    }
}

STATE_GARLAS_KEL = {
    id = 319
    subsistence_building = "building_subsistence_farms"
    provinces = { "x5E44C0" "x5E4C80" "xDD3640" "xDD4280" "xDE4A40" }
    traits = { "state_trait_harpy_hills" }
    city = "x5e44c0"
    farm = "xdd3640"
    mine = "x5e4c80"
    arable_land = 28
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations }
    capped_resources = {
        bg_iron_mining = 26
        bg_coal_mining = 16
        bg_logging = 4
    }
}

STATE_HARPYLEN = {
    id = 320
    subsistence_building = "building_subsistence_farms"
    provinces = { "x5D3880" "x5D3CFF" "x5D4040" "x5E4800" "x5E50FF" "xDD3AC0" "xDD3E00" "xDE46FF" "xDE4EC0" "xDF5200" }
    traits = { "state_trait_harpy_hills" }
    city = "x5e4800"
    farm = "xdf5200"
    mine = "x5d3cff"
    arable_land = 49
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_iron_mining = 12
        bg_coal_mining = 38
        bg_lead_mining = 22
        bg_sulfur_mining = 24 #Volcano meme Flamepeak as per Three Eruptions of Cannor. Change number if needed -Jay
        bg_logging = 6
    }
}

STATE_EAST_HARPY_HILLS = {
    id = 321
    subsistence_building = "building_subsistence_farms"
    provinces = { "x5F5440" "x5F58C0" "x5F5C00" "xCDA397" "xDF5680" "xDF5AFF" "xDF5E40" }
    traits = { "state_trait_harpy_hills" }
    city = "xdf5e40"
    farm = "xdf5aff"
    mine = "x5f58c0"
    arable_land = 20
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations }
    capped_resources = {
        bg_iron_mining = 12
        bg_coal_mining = 18
        bg_logging = 5
    }
}

STATE_FIRANYALEN = {
    id = 322
    subsistence_building = "building_subsistence_farms"
    provinces = { "x606080" "x6064FF" "x606840" "x606CC0" "xE062C0" "xE06600" "xE06A80" }
    traits = { "state_trait_harpy_hills" }
    city = "x606840"
    farm = "xe06600"
    wood = "xe062c0"
    mine = "x606cc0"
    arable_land = 41
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations }
    capped_resources = {
        bg_iron_mining = 12
        bg_logging = 12
    }
}

STATE_ARDU = {
    id = 323
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x4A3020" "x508C40" "x57CE5C" "x6178FF" "x79D413" "xD19640" "xD32EC0" "xD847F6" "xE17240" }
    traits = { "state_trait_salahad_desert" }
    city = "xd32ec0"
    farm = "x57ce5c"
    port = "x508c40"
    mine = "xd19640"
    arable_land = 31
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 12
        bg_lead_mining = 8
        bg_fishing = 4
    }
    naval_exit_id = 3300
}

STATE_KERUHAR = {
    id = 324
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x048A8B" "x31C47B" "x347B57" "x532EC9" "x5989F5" "x5E91A6" "xA6133C" "xC8A052" "xDAE55C" "xDB2001" "xE51650" }
    traits = { "state_trait_salahad_desert" }
    city = "xdae55c"
    farm = "xc8a052"
    port = "x048a8b"
    arable_land = 10
    arable_resources = { bg_livestock_ranches bg_coffee_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 4
    }
    naval_exit_id = 3300
}

STATE_FAR_EAST_SALAHAD = {
    id = 325
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x1E50DE" "x1FEC37" "xD5543F" "xDB2000" "xDBD26A" "xE31F3B" "xEFE169" }
    traits = { "state_trait_salahad_desert" }
    city = "xd5543f"
    farm = "xe31f3b"
    arable_land = 3
    arable_resources = { bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_EBBUSUBTU = {
    id = 326
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0E87A4" "x38CB14" "xD014AF" "xDC8A29" "xE8CDCA" }
    traits = { "state_trait_salahad_desert" }
    city = "x38cb14"
    farm = "xdc8a29"
    port = "xd014af"
    arable_land = 14
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_coffee_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 2
    }
    naval_exit_id = 3300
}

STATE_MULEN = {
    id = 327
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1566C9" "x519112" "x80BC16" "x8569D7" "xC8993F" "xD9E872" "xEFDD19" }
    traits = { "state_trait_salahad_desert" }
    city = "xc8993f"
    farm = "x8569d7"
    mine = "xd9e872"
    arable_land = 28
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_opium_plantations }
    capped_resources = {
        bg_sulfur_mining = 12
    }
}

STATE_ELAYENNA = {
    id = 328
    subsistence_building = "building_subsistence_farms"
    provinces = { "x492020" "xA0423D" "xC6A636" "xDB2C61" "xE12F05" "xE79365" "xF1CC6A" }
    traits = {}
    city = "xe79365"
    farm = "xc6a636"
    mine = "xdb2c61"
    arable_land = 27
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_opium_plantations }
    capped_resources = {
        bg_iron_mining = 12
    }
}

STATE_MASUSOPOT = {
    id = 329
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0D71BA" "x30A367" "x4784C0" "x478800" "x4890FF" "x544080" "xC78280" "xC786FF" "xC78A40" "xC89200" }
    traits = {}
    city = "xe79365"
    farm = "xc6a636"
    mine = "xdb2c61"
    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_ELIZNA = {
    id = 330
    subsistence_building = "building_subsistence_farms"
    provinces = { "x467880" "x467CFF" "x468040" "xC67AC0" "xC67E00" }
    traits = {}
    city = "xe79365"
    farm = "xc6a636"
    mine = "xdb2c61"
    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_KOROSHESH = {
    id = 331
    subsistence_building = "building_subsistence_farms"
    provinces = { "x423C80" "x4240FF" "x424440" "x4E6440" "xC23EC0" "xC24680" "xC78EC0" }
    traits = {}
    city = "xe79365"
    farm = "xc6a636"
    mine = "xdb2c61"
    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_NIRAT = {
    id = 332
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4248C0" "x434C00" "x435080" "x467400" "xB87D04" "xC24200" "xC34AFF" "xC34E40" "xC352C0" "xC67640" }
    traits = {}
    city = "xe79365"
    farm = "xc6a636"
    mine = "xdb2c61"
    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_OHITSOPOT = {
    id = 333
    subsistence_building = "building_subsistence_farms"
    provinces = { "x134218" "x402CFF" "x413040" "x413800" "x59D38A" "xC12E00" "xC13280" "xC136FF" "xC13A40" }
    traits = {}
    city = "xe79365"
    farm = "xc6a636"
    mine = "xdb2c61"
    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_KHETERAT = {
    id = 334
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4354FF" "x445840" "x445CC0" "x446000" "x478C80" "x94A960" "xC35600" "xC45A80" "xC45EFF" }
    traits = {}
    city = "xe79365"
    farm = "xc6a636"
    mine = "xdb2c61"
    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_IRSMAHAP = {
    id = 335
    subsistence_building = "building_subsistence_farms"
    provinces = { "x446480" "x4568FF" "x456C40" "x4570C0" "x6CFE68" "x897871" "xC46240" "xC566C0" "xC56A00" "xC56E80" "xC572FF" "xF02189" }
    traits = {}
    city = "xe79365"
    farm = "xc6a636"
    mine = "xdb2c61"
    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_SANDSPITE = {
    id = 336
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0AF557" "x886FF3" "x985AB7" "xE34957" "xF6C468" }
    traits = {}
    city = "xe79365"
    farm = "xc6a636"
    mine = "xdb2c61"
    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_ZITAN_TOMON = {
    id = 337
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0314BB" "x20E222" "x3A8D1D" "x5252F7" "x6C596D" "x6EC8DE" "x84DD25" "x8DF25B" "x944921" "xAAD34C" "xC96152" "xDCFD57" "xF19D40" }
    traits = {}
    city = "xe79365"
    farm = "xc6a636"
    mine = "xdb2c61"
    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {

    }
}

STATE_CRATHANOR = {
    id = 338
    subsistence_building = "building_subsistence_farms"
    provinces = { "x402880" "x4924FF" "x492840" "xC92600" "xC92A80" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 37
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_silk_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_coal_mining = 10
        bg_logging = 8
        bg_fishing = 6
    }
    naval_exit_id = 3002
}

STATE_OVDAL_TUNGR = {
    id = 339
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4B3C40" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 30
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_coal_mining = 10
        bg_logging = 8
        bg_fishing = 6
    }
    naval_exit_id = 3003
}
