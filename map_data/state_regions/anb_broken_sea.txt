﻿STATE_TRITHEMAR = {
    id = 295
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2A2DF9" "x374BEF" "x78955F" "x858CBF" "x939F2A" "x977A70" "xD81D58" "xE07B7B" }
    traits = {}
    city = "x374bef" #NEW PLACE
    port = "xd81d58" #Random
    farm = "x78955f" #Random
    wood = "x2a2df9" #Random
    arable_land = 28
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3112
}

STATE_REITHEMAR = {
    id = 296
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0700F0" "x1B3D7D" "x3ABC20" "x3B5C7E" "x6D6334" "xBA56AD" }
    traits = {}
    city = "x3abc20" #Random
    port = "x3b5c7e" #Random
    farm = "x0700f0" #Random
    wood = "xba56ad" #Random
    arable_land = 9
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3112
}

STATE_SILDARBAD = {
    id = 297
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0500E8" "x08B647" "x3D3ED8" "x434C7E" "x5D6A63" "x83D701" "x882F94" "x8D3309" "xB1EAC9" "xC3524F" "xD1F604" "xD48CEB" "xDAC4ED" "xFDEBB0" }
    traits = {}
    city = "x0500e8" #Sildarbad
    port = "x434c7e" #Random
    farm = "xd1f604" #Random
    wood = "x8d3309" #Random
    arable_land = 12
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3113
}

STATE_ELFEBENCODD = {
    id = 298
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x142AEF" "x30613B" "x5BAFDC" "xB5E1FE" }
    traits = {}
    city = "x142aef" #Random
    port = "x30613b" #Random
    farm = "xb5e1fe" #Random
    wood = "x5bafdc" #Random
    arable_land = 9
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3113
}

STATE_FARNOR = {
    id = 299
    subsistence_building = "building_subsistence_farms"
    provinces = { "x435820" "x445820" "x59D654" "xC356AD" "xC45A4F" }
    traits = {}
    city = "xc356ad" #Random
    port = "x445820" #Random
    farm = "xc45a4f" #Random
    wood = "x59d654" #Random
    arable_land = 17
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3113
}

STATE_HIERSILD = {
    id = 500
    subsistence_building = "building_subsistence_farms"
    provinces = { "x25D0EA" "x456C7E" "x471938" "x5A2781" "x644D92" "x708011" "xAA1FC4" "xBFD046" "xC408A8" "xC56A4F" }
    traits = {}
    city = "x456c7e" #Random
    farm = "xc408a8" #Random
    wood = "xc56a4f" #Random
    arable_land = 13
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_BROKEN_BAY = {
    id = 501
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x1C97AE" "x3A4C7E" "x3A547E" "x500F71" "x52CCD4" "xBA4EAD" "xBA524F" "xCBC752" "xF882AD" }
    traits = {}
    city = "xba524f" #Random
    port = "x3a4c7e" #Random
    farm = "xba4ead" #Random
    wood = "x1c97ae" #Random
    arable_land = 14
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3111
}

STATE_DALAIRRSILD = {
    id = 502
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0D35DB" "x2D8020" "x2E9020" "x347C7E" "x35FC7E" "x369C7E" "x36E150" "x39447E" "x39AC20" "x5F409A" "x633145" "x70B700" "x7B8DDF" "x7D8DDF" "xB2743E" "xB69EAD" "xB946AD" "xB94A4F" "xD7DA53" "xE82A37" "xF782AD" }
    traits = {}
    city = "xb94a4f" #Random
    farm = "x39447e" #Random
    wood = "x0d35db" #Random
    arable_land = 24
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 24
        bg_sulfur_mining = 12
    }
}

STATE_SJAVARRUST = {
    id = 503
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x18C602" "x33947E" "x362020" "x37247E" "x372820" "x372C7E" "x383020" "x818DDF" "x8588DF" "xB7224F" "xB72A4F" "xB7F6AD" "xCA6546" "xCE9158" "xD0956F" "xEBC180" }
    traits = {}
    city = "xce9158" #Random
    port = "x383020" #Random
    farm = "x372c7e" #Random
    wood = "x362020" #Random
    arable_land = 21
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 5
        bg_whaling = 2
        bg_logging = 8
        bg_iron_mining = 8
    }
    naval_exit_id = 3110
}

STATE_HJORDAL = {
    id = 504
    subsistence_building = "building_subsistence_farms"
    provinces = { "x348020" "x3DE150" "xC91DA7" "xD60BE9" "xDCDA53" }
    traits = {}
    city = "x348020" #Noo Oddansbay
    port = "xd60be9" #Random
    farm = "xdcda53" #Random
    wood = "x3de150" #Random
    arable_land = 34
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 6
        bg_logging = 12
        bg_whaling = 2
        bg_iron_mining = 20
    }
    naval_exit_id = 3109
}

STATE_FLOTTNORD = {
    id = 505
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x244684" "x32847E" "x358820" "x369820" "x65409A" "xB696AD" "xB69A4F" "xC51DA7" }
    traits = {}
    city = "x358820" #Random
    port = "xb696ad" #Random
    farm = "x369820" #Random
    wood = "xb69a4f" #Random
    arable_land = 15
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 6
        bg_whaling = 6
        bg_iron_mining = 28
    }
    naval_exit_id = 3110
}

STATE_MOLBROTIDD = {
    id = 506
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x61D155" "x7E6BFF" "xC25969" "xC2AAA3" "xD3A32E" }
    traits = {}
    city = "x61d155" #Random
    port = "xd3a32e" #Random
    farm = "x7e6bff" #Random
    wood = "xc25969" #Random
    arable_land = 8
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 3
        bg_whaling = 2
    }
    naval_exit_id = 3110
}

STATE_INUVUORLAK = {
    id = 507
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0C5609" "x383C7E" "x5ED155" "xB384FF" "xB836AD" "xB83A4F" "xB93EAD" "xE40539" }
    traits = {}
    city = "xb93ead" #Random
    port = "x383c7e" #Random
    farm = "xb83a4f" #Random
    wood = "xb836ad" #Random
    arable_land = 9
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 5
        bg_logging = 6
    }
    naval_exit_id = 3111
}

STATE_NYHOFN = {
    id = 508
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x11D353" "xB784FF" "xBECF32" "xCE6546" }
    traits = {}
    city = "xce6546" #Random
    port = "xb784ff" #Random
    farm = "x11d353" #Random
    wood = "xbecf32" #Random
    arable_land = 9
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 3
        bg_whaling = 2
    }
    naval_exit_id = 3110
}

STATE_DALAIREY_WASTES = {
    id = 509
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x04DB73" "x19A814" "x2C2F3C" "x3002FF" "x31C090" "x3BEB2E" "x443E92" "x4A347E" "x4B3C7E" "x6CF948" "x745CF8" "x8018A3" "x828DDF" "x85E56B" "x8D241F" "x90FE7E" "xB04190" "xB07F50" "xC99759" "xD2956F" "xDC67F7" "xF74125" }
    traits = { state_trait_dalairey_wastes } #to prevent early colonization





    city = "xc99759" #Random
    port = "x4b3c7e" #Random
    farm = "xd2956f" #Random
    wood = "x828ddf" #Random
    arable_land = 5
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 2
        bg_whaling = 2
        bg_sulfur_mining = 50
    }
    naval_exit_id = 3109
}

STATE_FORLORN_ISLE = {
    id = 510
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x105609" "xA0C2BF" "xA874E1" "xB586AD" }
    traits = {}
    city = "xb586ad" #Random
    port = "x105609" #Random
    farm = "xA0C2BF" #Random
    wood = "xA874E1" #Random
    arable_land = 12
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 3
        bg_whaling = 2
    }
    naval_exit_id = 3109
}

STATE_FIORGAM = {
    id = 511
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0C547E" "x108820" "x1D00F8" "x8A56AD" "x9086AD" "x908A4F" }
    traits = {}
    city = "x0c547e" #Random
    port = "x9086ad" #Random
    farm = "x108820" #Random
    wood = "x908a4f" #Random
    arable_land = 22
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 4
        bg_whaling = 5
    }
    naval_exit_id = 3101
}

STATE_MITTANWEK = {
    id = 512
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x2FAE4E" "x35D461" "x84176B" "x894B71" "xABCC1E" }
    traits = {}
    city = "x894B71" #NEW PLACE
    port = "x2fae4e" #Random
    farm = "x35d461" #Random
    wood = "xabcc1e" #Random
    arable_land = 11
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3112
}

STATE_FARPLOTT = {
    id = 513
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x00821E" "x8848C6" "x90C050" "xA7AE88" "xF0CA34" "xF2263F" }
    traits = {}
    city = "xa7ae88" #Random
    port = "x8848C6" #Random
    farm = "x90c050" #Random
    wood = "xf0ca34" #Random
    arable_land = 10
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3112
}

STATE_STERNGCOTTOPCOTT = {
    id = 514
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x383820" "x38FC7E" "x39A420" "x5B5500" "x878A7C" "xB8324F" }
    traits = {}
    city = "x39a420" #Random
    port = "x383820" #Random
    farm = "x38fc7e" #Random
    wood = "xb8324f" #Random
    arable_land = 7
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3111
}

STATE_ARBCYRR = {
    id = 515
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x1F405F" "x2D0CA4" "x672B93" "x7E59EB" "xD4ABBA" }
    traits = {}
    city = "x1f405f" #Random
    port = "x7e59eb" #Random
    farm = "x2d0ca4" #Random
    wood = "xd4abba" #Random
    arable_land = 6
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3111
}

STATE_HAMMALPODDODDOD = {
    id = 516
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x1A5ED2" "x467C7E" "x6A00EC" "xC67A4F" }
    traits = {}
    city = "x1a5ed2" #Random
    port = "x467c7e" #Random
    farm = "xc67a4f" #Random
    wood = "x6a00ec" #Random
    arable_land = 11
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3113
}

STATE_PORKETCOTTOPCOTT = {
    id = 517
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0078F4" "x21A4C8" "x31F03F" "x9DA2FB" "xA0A058" "xB72711" "xF8B9BC" }
    traits = {}
    city = "x0078f4" #Random
    port = "x21a4c8" #Random
    farm = "xa0a058" #Random
    wood = "xb72711" #Random
    arable_land = 7
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3114
}

STATE_GRACOST = {
    id = 518
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0700F1" "x393FDF" "xAA6512" "xBB5A4F" "xBB5EAD" }
    traits = {}
    city = "xbb5ead" #Random
    port = "x393fdf" #Random
    farm = "xbb5a4f" #Random
    wood = "xaa6512" #Random
    arable_land = 10
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3113
}

STATE_LANCEYNN = {
    id = 519
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0F72F3" "x1C93E4" "x35839B" "x3C651C" "x3F05E8" "x555B2E" "x9806EE" "xD23718" }
    traits = {}
    city = "x35839b" #Random
    farm = "x555b2e" #Random
    wood = "x3c651c" #Random
    arable_land = 21
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_GROONCAMB = {
    id = 520
    subsistence_building = "building_subsistence_farms"
    provinces = { "x34847E" "x66409A" "xB4824F" "xBE8BB1" "xD92880" }
    traits = {}
    city = "xb4824f" #Random
    port = "x66409a" #Random
    farm = "xd92880" #Random
    wood = "x34847e" #Random
    arable_land = 33
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 4
        bg_logging = 12
        bg_whaling = 2
        bg_coal_mining = 28
    }
    naval_exit_id = 3109
}

STATE_CERRICK = {
    id = 521
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1BC602" "x358C7E" "x359020" "x9BF036" "xB58EAD" "xEBCAEF" "xEE2A37" "xFF82AD" }
    traits = {}
    city = "xebcaef" #Random
    farm = "x9bf036" #Random
    wood = "x359020" #Random
    arable_land = 47
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 14
        bg_iron_mining = 16
    }
}

STATE_SILDGEELDROY = {
    id = 522
    subsistence_building = "building_subsistence_farms"
    provinces = { "x264684" "x273D46" "x362CB9" "x36947E" "x522FE7" "x693145" "xB47EAD" "xB5924F" "xC3E78C" "xEFC180" }
    traits = {}
    city = "x693145" #Random
    farm = "xb5924f" #Random
    wood = "x362cb9" #Random
    arable_land = 16
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 18
        bg_iron_mining = 14
    }
}

STATE_EAST_ARBERITH_WASTES = {
    id = 523
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x107763" "x1617DD" "x30E94A" "xB00090" "xB40258" "xCB424F" "xE70539" "xE855B1" "xF68D31" }
    traits = {}
    city = "xcb424f" #Random
    port = "xf68d31" #Random
    farm = "xe855b1" #Random
    wood = "xe70539" #Random
    arable_land = 1
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3112
}

STATE_WEST_ARBERITH_WASTES = {
    id = 524
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0F1339" "x2B18E0" "x4A0914" "x6D25DC" "x788B48" "x97865C" "xFC2A57" }
    traits = {}
    city = "x6d25dc" #Random
    port = "x2b18e0" #Random
    farm = "x788b48" #Random
    wood = "x0f1339" #Random
    arable_land = 1
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3114
}
