﻿STATE_DAMESHEAD = {
    provinces = { "x08fafa" }
    id = 3000
}

STATE_WEST_DIVEN = {
    provinces = { "x69e1ff" }
    id = 3001
}

STATE_CENTRAL_DIVEN = {
    provinces = { "x3ac8fa" }
    id = 3002
}

STATE_EAST_DIVEN = {
    provinces = { "x38fafa" }
    id = 3003
}

STATE_BAY_OF_WINES = {
    provinces = { "x30fafa" }
    id = 3004
}

STATE_TEFKORA_PASS = {
    provinces = { "x5dcaca" }
    id = 3005
}

STATE_WESTCOAST = {
    provinces = { "x24fafa" }
    id = 3006
}

STATE_GIANTS_GRAVE_SEA = {
    provinces = { "x49c8c8" }
    id = 3007
}

STATE_FROZEN_PASS = {
    provinces = { "x5cfbfb" }
    id = 3008
}

STATE_NORTH_REAVERS_SEA = {
    provinces = { "x29c8c8" }
    id = 3009
}

STATE_SOUTH_REAVERS_SEA = {
    provinces = { "x63ddfb" }
    id = 3010
}

STATE_DAMESNECK = {
    provinces = { "x16c8fa" }
    id = 3011
}

STATE_COAST_OF_VENAIL = {
    provinces = { "x10fafa" }
    id = 3012
}

STATE_DIVENGATE = {
    provinces = { "x5ecafc" }
    id = 3013
}

STATE_NORTHERN_THAW = {
    provinces = { "x37ddfb" }
    impassable = { "x37ddfb" }
    id = 3050
}

STATE_WHALEROAD = {
    provinces = { "x69caca" }
    id = 3051
}

STATE_LONELY_ISLE_SEA = {
    provinces = { "x3ecefc" }
    id = 3052
}

STATE_DRAGONS_BREATH_SEA = {
    provinces = { "x68fbfb" }
    id = 3053
}

STATE_UELOS_LAMENT_1 = {
    provinces = { "x27dcfa" }
    impassable = { "x27dcfa" }
    id = 3054
}

STATE_UELOS_LAMENT_2 = {
    provinces = { "x2bdcfb" }
    impassable = { "x2bdcfb" }
    id = 3055
}

STATE_LONELY_FAR_LANE_3 = {
    provinces = { "x5cfcfc" }
    id = 3056
}

STATE_FAR_ISLE_COAST = {
    provinces = { "x32c9fb" }
    id = 3057
}

STATE_UELOS_LAMENT_4 = {
    provinces = { "x5acbfd" }
    impassable = { "x5acbfd" }
    id = 3059
}

STATE_EDILLIONS_RELIEF = {
    provinces = { "x3dcaca" }
    impassable = { "x3dcaca" }
    id = 3060
}

STATE_FAR_ISLE_COAST_LANE_1 = {
    provinces = { "x78fbfb" }
    id = 3061
}

STATE_FAR_ISLE_COAST_LANE_2 = {
    provinces = { "x3cfbfb" }
    id = 3062
}

STATE_DEVILS_LANE = {
    provinces = { "x57defc" }
    id = 3063
}

STATE_EDILLIONS_RELIEF_LANE = {
    provinces = { "x0dfee0" }
    id = 3064
}

STATE_UELOS_LAMENT_5 = {
    provinces = { "x49caca" }
    impassable = { "x49caca" }
    id = 3066
}

STATE_CROWNSMANS_CURRENT = {
    provinces = { "x6bddfb" }
    id = 3067
}

STATE_UELOS_LAMENT_6 = {
    provinces = { "x55caca" }
    impassable = { "x55caca" }
    id = 3068
}

STATE_BANISHED_SEA = {
    provinces = { "x00fcfc" }
    id = 3100
}

STATE_SILSENSALD_SEA = {
    provinces = { "x7EC9FB" }
    id = 3101
}

STATE_CALAMITY_PASS = {
    provinces = { "x03defc" }
    id = 3102
}

STATE_ENDRALS_WAY = {
    provinces = { "x0dcacb" }
    id = 3103
}

STATE_OCHCHUS_SEA = {
    provinces = { "x3fddfb" }
    id = 3104
}

STATE_RUINED_SEA = {
    provinces = { "x1cfcfc" }
    id = 3105
}

STATE_REAPERS_COAST = {
    provinces = { "x25cbcb" }
    id = 3106
}

STATE_SEA_OF_RESPITE = {
    provinces = { "x6acbfd" }
    id = 3107
}

STATE_TROLLSBAY_COAST = {
    provinces = { "x0ecafc" }
    id = 3108
}

STATE_ICEBERG_ALLEY = {
    provinces = { "x4cfcfc" }
    id = 3109
}

STATE_ICEBREAKER_PASS = {
    provinces = { "x5dc9c9" }
    id = 3110
}

STATE_EASTERN_FLOEWAY = {
    provinces = { "x77ddfb" }
    id = 3111
}

STATE_WESTERN_FLOEWAY = {
    provinces = { "x6fddfb" }
    id = 3112
}

STATE_BAY_OF_VERENCEL = {
    provinces = { "x73ddfb" }
    id = 3113
}

STATE_TORRIARAN = {
    provinces = { "x6ec9fb" }
    id = 3114
}

STATE_EIGRAS = {
    provinces = { "x3cfdfd" }
    id = 3115
}

STATE_TORN_SEA = {
    provinces = { "x36cbfd" }
    id = 3116
}

STATE_TORN_SEA_2 = {
    provinces = { "x72cbfd" }
    impassable = { "x72cbfd" }
    id = 3117
}

STATE_TORN_SEA_3 = {
    provinces = { "x6bdffd" }
    impassable = { "x6bdffd" }
    id = 3118
}

STATE_ARTIFICERS_COAST = {
    provinces = { "x38fcfc" }
    id = 3120
}

STATE_GULF_OF_NEANAANI = {
    provinces = { "x2dcbcb" }
    id = 3121
}

STATE_SARMADFAR = {
    provinces = { "x32cbfd" }
    id = 3122
}

STATE_FOGHARABH = {
    provinces = { "x39cbcb" }
    id = 3123
}

STATE_LOVERS_PASS = {
    provinces = { "x60fdfd" }
    id = 3124
}

STATE_MATNI_SEA = {
    provinces = { "x68cbcb" }
    id = 3125
}

STATE_KAYDHANO_COAST = {
    provinces = { "x31caca" }
    id = 3126
}

STATE_CLEAVED_SEA = {
    provinces = { "x4cfdfd" }
    id = 3127
}

STATE_THENON_CURRENT = {
    provinces = { "x5dcbcb" }
    id = 3128
}

STATE_SEA_OF_AMEION = {
    provinces = { "x6cfdfd" }
    id = 3129
}

STATE_RADIANT_SEA = {
    provinces = { "x25caca" }
    id = 3130
}

STATE_GLEAMING_SEA = {
    provinces = { "x56cbfd" }
    id = 3131
}

STATE_TURTLEBACK_SEA = {
    provinces = { "x22cafc" }
    id = 3132
}

STATE_ELTCHAMAS_PASS_1 = {
    provinces = { "x2ecafc" }
    id = 3133
}

STATE_CLEAVED_SEA_DEPARTURE_LANE = {
    provinces = { "x1fdefc" }
    id = 3134
}

STATE_SEA_OF_SILENCE_LANE_1 = {
    provinces = { "x340eec" }
    id = 3135
}

STATE_SEA_OF_SILENCE_LANE_2 = {
    provinces = { "xa729a6" }
    id = 3136
}

STATE_ADDERSID_LANE = {
    provinces = { "x2d4f87" }
    id = 3137
}

STATE_SEA_OF_SILENCE_LANE_3 = {
    provinces = { "xe10dad" }
    id = 3138
}

STATE_SEA_OF_SILENCE = {
    provinces = { "x49c9c9" }
    impassable = { "x49c9c9" }
    id = 3142
}

STATE_REIAN_MARLOS = {
    provinces = { "x445572" }
    impassable = { "x445572" }
    id = 3143
}

STATE_EOR_ISLANDS_SEA = {
    provinces = { "x4acbfd" }
    id = 3144
}

STATE_KELPFOREST_SEA = {
    provinces = { "x64fdfd" }
    impassable = { "x64fdfd" }
    id = 3145
}

STATE_GATHMARKORSK_SEA = {
    provinces = { "x16fafa" }
    impassable = { "x16fafa" }
    id = 3146
}

STATE_TORN_SEA_4 = {
    provinces = { "x3b70b1" }
    impassable = { "x3b70b1" }
    id = 3147
}
STATE_WRECKS_SEA_LANE_1 = {
    provinces = { "x0dc9c9" }
    id = 3148
}

STATE_JADD_SEA = {
    provinces = { "x63dcfa" }
    id = 3300
}

STATE_GULF_OF_RAHEN = {
    provinces = { "x70fafa" }
    id = 3301
}

STATE_KHOM_SEA = {
    provinces = { "x6acafc" }
    id = 3302
}

STATE_RINGLET_BASIN = {
    provinces = { "x0bdffd" }
    id = 3303
}

STATE_PHOKHAO_PASS = {
    provinces = { "x79c8c8" }
    id = 3304
}

STATE_JELLYFISH_COAST = {
    provinces = { "x48fdfd" }
    id = 3305
}

STATE_ODHEONGU_SEA = {
    provinces = { "x7cfafa" }
    id = 3306
}

STATE_WIDOWS_SEA = {
    provinces = { "x7fdcfa" }
    id = 3307
}

STATE_HOKHOS_SEA = {
    provinces = { "x88fdfd" }
    id = 3308
}

STATE_BLUE_SEA = {
    provinces = { "x03ddfb" }
    id = 3309
}

STATE_KODARVE_LAKE = {
    provinces = { "x29d8e4" }
    id = 3310
}

STATE_YUKELQUR_LAKE = {
    provinces = { "x349df9" }
    id = 3311
}

STATE_ORIOLG_CHANNEL = {
    provinces = { "x41c9c9" }
    id = 3312
}

STATE_ZERNUUK_LAKE = {
    provinces = { "x7edefc" }
    id = 3313
}

STATE_THE_BRIDGE = {
    provinces = { "x2ac9fb" }
    impassable = { "x2ac9fb" }
    id = 3314
}

STATE_KEDWALI_GULF = {
    provinces = { "x56c8fa" }
    id = 3400
}

STATE_EAST_SARHAL_COAST = {
    provinces = { "x6dcaca" }
    id = 3401
}

STATE_PUNCTURED_COAST = {
    provinces = { "x51c8c8" }
    id = 3402
}

STATE_KEDWALI_COAST = {
    provinces = { "x78fcfc" }
    id = 3403
}
STATE_FAHVANOSY_SEA = {
    provinces = { "x62cafc" }
    id = 3404
}
STATE_DAO_NAKO_COAST = {
    provinces = { "x70fcfc" }
    id = 3405
}

STATE_SANDSPITE_SEA = {
    provinces = { "x50fcfc" }
    id = 3406
}
STATE_VOYAGERS_SEA = {
    provinces = { "x7acafc" }
    id = 3407
}
STATE_SOUTH_UELOS_LAMENT_1 = {
    provinces = { "x7fdefc" }
    impassable = { "x7fdefc" }
    id = 3408
}
STATE_SOUTH_UELOS_LAMENT_2 = {
    provinces = { "x59c9c9" }
    impassable = { "x59c9c9" }
    id = 3409
}
STATE_SOUTH_UELOS_LAMENT_3 = {
    provinces = { "x7dcaca" }
    impassable = { "x7dcaca" }
    id = 3410
}
STATE_VOYAGERS_SEA_LANE_1 = {
    provinces = { "x03dffd" }
    id = 3411
}
STATE_VOYAGERS_SEA_LANE_2 = {
    provinces = { "x28fdfd" }
    id = 3412
}

STATE_ARDIMYAN_SEA = {
    provinces = { "x7ecafc" }
    id = 3413
}

STATE_INSYAA_SEA_1 = {
    provinces = { "x01cbcb" }
    id = 3500
}

STATE_INSYAA_SEA_2 = {
    provinces = { "x28fbfb" }
    id = 3501
}

STATE_BARRIER_ISLANDS_SEA = {
    provinces = { "x1fddfb" }
    id = 3502
}

STATE_BAY_OF_INSYAA = {
    provinces = { "x6bdcfa" }
    id = 3503
}

STATE_INSYAA_SEA_3 = {
    provinces = { "x74fcfc" }
    id = 3504
}

STATE_SOUTHERN_OCEAN_OF_THE_LOST_1 = {
    provinces = { "x66cbfd" }
    impassable = { "x66cbfd" }
    id = 3505
}

STATE_ELTCHAMAS_PASS_2 = {
    provinces = { "x341aba" }
    id = 3506
}

STATE_SOUTHERN_OCEAN_OF_THE_LOST_2 = {
    provinces = { "x20cce0" }
    impassable = { "x20cce0" }
    id = 3507
}

STATE_OCEAN_OF_THE_LOST_2 = {
    provinces = { "x405b92" }
    impassable = { "x405b92" }
    id = 3509
}

STATE_NORTHERN_OCEAN_OF_THE_LOST = {
    provinces = { "x25c9c9" }
    impassable = { "x25c9c9" }
    id = 3510
}

STATE_JERKHICH_SEA = {
    provinces = { "x23dffd" }
    impassable = { "x23dffd" }
    id = 3511
}

STATE_MUDFAIBH_PASS_2 = {
    provinces = { "x721b9a" }
    id = 3512
}

STATE_MUDFAIBH_PASS_3 = {
    provinces = { "x2861cb" }
    id = 3513
}

STATE_BLATHFAR_PASS_1 = {
    provinces = { "x0d61a4" }
    id = 3514
}

STATE_BLATHFAR_PASS_2 = {
    provinces = { "x5ecbfd" }
    id = 3515
}

STATE_SEA_OF_SILENCE_LANE_4 = {
    provinces = { "x6147c1" }
    id = 3516
}

STATE_MUDFAIBH_PASS_1 = {
    provinces = { "x316868" }
    id = 3517
}
STATE_THEMSEA = {
    provinces = { "x17ddfb" }
    id = 3518
}

STATE_TORRIEANACH_SEA = {
    provinces = { "x54e2e1" }
    id = 3519
}
